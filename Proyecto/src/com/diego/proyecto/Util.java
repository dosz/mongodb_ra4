package com.diego.proyecto;


import com.diego.proyecto.GUI.Ventana;

import javax.swing.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by dos_6 on 08/02/2016.
 */
public class Util {

    Ventana view;

    public static String formatMoneda(float cantidad) {

        DecimalFormat decimalFormat =
                new DecimalFormat("#.00 �");

        return decimalFormat.format(cantidad);
    }

    public static float unFormatMoneda(String cantidad)
            throws ParseException {

        DecimalFormat decimalFormat =
                new DecimalFormat("#.00 �");

        return decimalFormat.parse(cantidad).floatValue();
    }

    public static void mensajeInformacion(String titulo,
                                          String mensaje) {

        JOptionPane.showMessageDialog(null,
                mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);

    }

    public static void mensajeError(String titulo,
                                    String mensaje) {

        JOptionPane.showMessageDialog(null,
                mensaje, titulo, JOptionPane.ERROR_MESSAGE);

    }

    public static int mensajeConfirmacion(String titulo,
                                          String mensaje) {

        return JOptionPane.showConfirmDialog(null,
                mensaje, titulo, JOptionPane.YES_NO_OPTION);
    }

    public void mensajeEstado(String mensaje){
        view.lbEstado.setText(mensaje);
    }

    public static String formatFecha(Date fecha) {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        return format.format(fecha);
    }

    public static Date parseFecha(String fecha) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        return format.parse(fecha);
    }

}
