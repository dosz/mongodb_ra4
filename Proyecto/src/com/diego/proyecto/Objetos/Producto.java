package com.diego.proyecto.Objetos;

import org.bson.types.ObjectId;

import javax.persistence.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;
/**
 * Created by dos_6 on 05/02/2016.
 */
@Entity
@Table(name="productos")
public class Producto implements Serializable{

    public static String COLECCION = "productos";

    private int _id;

    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "precio_base")
    private float precioBase;

    @OneToMany(mappedBy = "producto")
    List<Empresa> listaEmpresa;

    public Producto(){
    }

    public Producto(int id, String nombre, float precioBase) {
        this.id = id;
        this.nombre = nombre;
        this.precioBase = precioBase;
        this.listaEmpresa = new ArrayList<>();
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(float precioBase) {
        this.precioBase = precioBase;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
