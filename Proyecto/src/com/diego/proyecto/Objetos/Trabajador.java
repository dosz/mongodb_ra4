package com.diego.proyecto.Objetos;

import org.bson.types.ObjectId;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dos_6 on 03/02/2016.
 */
@Entity
@Table(name = "trabajadores")
public class Trabajador implements Serializable {

    public static String COLECCION = "trabajadores";

    private int _id;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellidos")
    private String apellidos;
    @Column(name = "nacimiento")
    private Date nacimiento;
    @Column(name = "telefono")
    private String telefono;

    @ManyToOne
    @JoinColumn(name = "id_empresa")
    Empresa empresaAsignada;

    public Trabajador(){}

    public Trabajador(int _id, String nombre,
                      String apellidos, Date nacimiento, String telefono) {
        this._id = _id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nacimiento = nacimiento;
        this.telefono = telefono;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public Empresa getEmpresaAsignada() {
        return empresaAsignada;
    }

    public void setEmpresaAsignada(Empresa empresaAsignada) {
        this.empresaAsignada = empresaAsignada;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return nombre +" "+apellidos;
    }
}
