package com.diego.proyecto.Objetos;


import org.bson.types.ObjectId;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Tabla para los Usuarios.
 * TODO login
 */

@Entity
@Table(name="usuarios")
public class Usuario{

    public static String COLECCION = "usuarios";

    private ObjectId _id;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "password")
    private String password;

    public Usuario(){}

    public Usuario(int id, String nombre, String password){
        this.id = id;
        this.nombre = nombre;
        this.password = password;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
