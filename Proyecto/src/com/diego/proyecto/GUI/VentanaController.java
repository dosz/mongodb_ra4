package com.diego.proyecto.GUI;

import com.diego.proyecto.Objetos.Empresa;
import com.diego.proyecto.Objetos.Producto;
import com.diego.proyecto.Objetos.Trabajador;
import com.diego.proyecto.Objetos.Usuario;
import com.diego.proyecto.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dos_6 on 03/02/2016.
 */
public class VentanaController{

    Ventana view;
    VentanaModel model;
    Producto producto;
    Empresa empresa;
    Trabajador trabajador;
    Usuario usuario;
    Util util;

    public VentanaController(Ventana view, VentanaModel model) {
        this.view = view;
        this.model = model;
        listeners();
        model.conectar();
        model.conectarMongo();
    }

    public void inicializar() throws ParseException {
        apagarBotones(true);
        listarProducto();
        listarEmpresa();
        listarTrabajadores();
    }

    public void desconectar(){
        model.desconectar();
        model.desconectarMongo();
    }

    public void listeners(){
        view.btProductoNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                iniciarBotonProducto(false);
            }
        });

        view.btProductoGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                producto = new Producto();
                producto.setNombre(view.tfProductoNombre.getText());
                producto.setPrecioBase(Float.parseFloat(view.tfProductoPrecio.getText()));
                try {
                    model.addProducto(producto);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                try {
                    listarProducto();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                iniciarBotonProducto(false);
                view.lbEstado.setText("Producto guardado con exito");
                limpiarCajas();
            }
        });

        view.btProductoModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int fila = view.tablaProducto.getSelectedRow();
                int id = (int) view.tablaProducto.getValueAt(fila,0);
                try {
                    producto = model.getProducto(id);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                producto.setNombre(view.tfProductoNombre.getText());
                producto.setPrecioBase(Float.parseFloat(view.tfProductoPrecio.getText()));
                if(util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                    return;
                model.updateProducto(producto);
                view.lbEstado.setText("Producto actualizado correctamente");
                try {
                    listarProducto();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                limpiarCajas();
            }
        });

        view.btProductoCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limpiarCajas();
            }
        });

        view.btProductoEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaProducto.getSelectedRow();
                String nombre = (String) view.tablaProducto.getValueAt(fila,1);
                if(util.mensajeConfirmacion("Eliminar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                    return;
                model.eliminarProducto(nombre);
                view.lbEstado.setText("Producto eliminado correctamente");
                try {
                    listarProducto();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }


                limpiarCajas();
            }
        });

        view.btEmpresaNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                iniciarBotonEmpresa(false);
            }
        });

        view.btEmpresaGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                empresa = new Empresa();
                empresa.setNombre(view.tfEmpresaNombre.getText());
                empresa.setTipo(view.cbEmpresaTipo.getSelectedItem().toString());
                empresa.setDireccion(view.tfEmpresaDireccion.getText());
                if(view.tfEmpresaTelefono.getText().length()>9){
                    Util.mensajeError("Error","Error en el telefono, sobran caracteres");
                    return;
                }else if(view.tfEmpresaTelefono.getText().length()<9){
                    Util.mensajeError("Error","Error en el telefono, faltan caracteres");
                    return;
                }
                empresa.setTelefono(view.tfEmpresaTelefono.getText());
                empresa.setFundacion(view.dcEmpresaFundacion.getDate());
                empresa.setProducto((Producto) view.cbEmpresaProducto.getSelectedItem());
                System.out.println(empresa.getProducto().getNombre());
                try {
                    model.addEmpresa(empresa);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                view.lbEstado.setText("Empresa guardada correctamente");

                iniciarBotonEmpresa(true);
                try {
                    listarEmpresa();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                limpiarCajas();
            }
        });

        view.btEmpresaModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaEmpresa.getSelectedRow();
                int id = (int) view.tablaEmpresa.getValueAt(fila,0);
                Empresa empresa = null;
                try {
                    empresa = model.getEmpresa(id);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                empresa.setNombre(view.tfEmpresaNombre.getText());
                empresa.setTipo(view.cbEmpresaTipo.getSelectedItem().toString());
                empresa.setDireccion(view.tfEmpresaDireccion.getText());
                if(view.tfEmpresaTelefono.getText().length()>9){
                    Util.mensajeError("Error","Error en el telefono, sobran caracteres");
                    return;
                }else if(view.tfEmpresaTelefono.getText().length()<9){
                    Util.mensajeError("Error","Error en el telefono, faltan caracteres");
                    return;
                }
                empresa.setTelefono(view.tfEmpresaTelefono.getText());
                empresa.setFundacion(view.dcEmpresaFundacion.getDate());
                empresa.setProducto((Producto) view.cbEmpresaProducto.getSelectedItem());
                if(util.mensajeConfirmacion("Modificar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                    return;
                model.updateEmpresa(empresa);
                try {
                    listarEmpresa();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                view.lbEstado.setText("Empresa actualizada correctamente");
                limpiarCajas();
            }
        });

        view.btEmpresaCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                limpiarCajas();
            }
        });

        view.btEmpresaEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaEmpresa.getSelectedRow();
                String nombre = (String) view.tablaEmpresa.getValueAt(fila,1);
                if(util.mensajeConfirmacion("Eliminar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                    return;
                model.eliminarEmpresa(nombre);
                view.lbEstado.setText("Empresa eliminada correctamente");
                try {
                    listarEmpresa();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                limpiarCajas();
            }
        });

        view.btTrabajadorNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inicarBotonTrabajador(false);
            }
        });

        view.btTrabajadorGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    trabajador = new Trabajador();
                    trabajador.setNombre(view.tfTrabajadorNombre.getText());
                    if (view.tfTrabajadorTelefono.getText().length() > 9) {
                        Util.mensajeError("Error", "Error en el telefono, sobran caracteres");
                        return;
                    } else if (view.tfTrabajadorTelefono.getText().length() < 9) {
                        Util.mensajeError("Error", "Error en el telefono, faltan caracteres");
                        return;
                    }
                    trabajador.setTelefono(view.tfTrabajadorTelefono.getText());
                    trabajador.setApellidos(view.tfTrabajadorApellidos.getText());
                    trabajador.setNacimiento(view.dtTrabajadorNacimiento.getDate());
                    empresa = (Empresa) view.cbTrabajadorEmpresa.getSelectedItem();
                    trabajador.setEmpresaAsignada(empresa);
                    System.out.println(empresa);
                    try {
                        model.addTrabajador(trabajador);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }catch (Exception ex){
                    view.lbEstado.setText("Error inesperado");
                }
                view.lbEstado.setText("Trabajador guardado con exito");
                listarTrabajadores();
                limpiarCajas();
            }
        });

        view.btTrabajadorModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int fila = view.tablaTrabajador.getSelectedRow();
                    int id = (int) view.tablaTrabajador.getValueAt(fila, 0);
                    try {
                        trabajador = model.getTrabajador(id);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    trabajador.setNombre(view.tfTrabajadorNombre.getText());
                    trabajador.setApellidos(view.tfTrabajadorApellidos.getText());
                    trabajador.setNacimiento(view.dtTrabajadorNacimiento.getDate());
                    trabajador.setTelefono(view.tfTrabajadorTelefono.getText());
                    empresa = (Empresa) view.cbTrabajadorEmpresa.getSelectedItem();
                    trabajador.setEmpresaAsignada(empresa);
                    model.updateTrabajador(trabajador);
                }catch (Exception ex){
                    view.lbEstado.setText("Error inesperado");
                }
                view.lbEstado.setText("Trabajador modificado con exito");
                listarTrabajadores();
                limpiarCajas();
            }
        });

        view.btTrabajadorCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limpiarCajas();
            }
        });

        view.btTrabajadorEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = view.tablaTrabajador.getSelectedRow();
                String nombre = (String) view.tablaTrabajador.getValueAt(fila,1);
                if(util.mensajeConfirmacion("Eliminar", "¿Está seguro?") == JOptionPane.NO_OPTION)
                    return;
                try{
                    model.eliminarTrabajador(nombre);
                }catch (Exception ex){
                    view.lbEstado.setText("Error de algo");
                }
                view.lbEstado.setText("Trabajador eliminado con exito");
                listarTrabajadores();
                limpiarCajas();
            }
        });

        view.botonConectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    login();
            }
        });

        view.botonDesconectar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    desconectar();
                    apagarBotones(true);
                    view.lbEstado.setText("Desconectado Correctamente");
            }
        });

        view.botonRegistro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registroUsuario();
            }
        });

        view.botonExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    model.XMLEmpresa(model.getEmpresas());
                    model.XMLTrabajador(model.getTrabajadores());
                    model.XMLProducto(model.getProductos());
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
        });

        view.botonImportarSQL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    List<Producto> listaHibernate = model.getProductoHibernate();
                    for(Producto producto:listaHibernate){
                        model.addProducto(producto);
                    }
                    listarProducto();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
        });

        view.botonSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                desconectar();
                System.exit(JFrame.EXIT_ON_CLOSE);
            }
        });

        view.botonSQL.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    List<Producto>  exportarProductos = model.getProductos();
                    for(Producto producto : exportarProductos){
                        model.guardarHIbernate(producto);
                       Thread.sleep(500);
                    }
                } catch (ParseException e1) {
                    e1.printStackTrace();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        });

        view.tablaProducto.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(view.tablaProducto.getSelectedRow() == -1)
                    return;
                int fila = view.tablaProducto.getSelectedRow();
                int id = (int) view.tablaProducto.getValueAt(fila,0);
                try {
                    producto = model.getProducto(id);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                view.tfProductoNombre.setText(producto.getNombre());
                view.tfProductoPrecio.setText(String.valueOf(producto.getPrecioBase()));
            }
        });

        view.tablaEmpresa.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(view.tablaEmpresa.getSelectedRow() == -1)
                    return;
                int fila = view.tablaEmpresa.getSelectedRow();
                int id = (int) view.tablaEmpresa.getValueAt(fila,0);
                try {
                    empresa = model.getEmpresa(id);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                view.tfEmpresaNombre.setText(empresa.getNombre());
                view.tfEmpresaTelefono.setText(empresa.getTelefono());
                view.tfEmpresaDireccion.setText(empresa.getDireccion());
                view.cbEmpresaTipo.setSelectedItem(empresa.getTipo());
                view.dcEmpresaFundacion.setDate(empresa.getFundacion());
            }
        });

        view.tablaTrabajador.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (view.tablaTrabajador.getSelectedRow() == -1)
                    return;

                int fila = view.tablaTrabajador.getSelectedRow();
                int id = (int) view.tablaTrabajador.getValueAt(fila, 0);
                try {
                    trabajador = model.getTrabajador(id);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                view.tfTrabajadorNombre.setText(trabajador.getNombre());
                view.tfTrabajadorApellidos.setText(trabajador.getApellidos());
                view.tfTrabajadorTelefono.setText(trabajador.getTelefono());
                view.cbTrabajadorEmpresa.setSelectedItem(trabajador.getEmpresaAsignada().getNombre());
                view.dtTrabajadorNacimiento.setDate(trabajador.getNacimiento());
            }
        });

        view.tfBuscarProducto.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (view.tfBuscarProducto.getText().length() > 0) {
                    try {
                        listarProducto(model.buscarProducto(view.tfBuscarProducto.getText().toString()));

                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }else{
                    try {
                        listarProducto();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        view.tfBuscarEmpresa.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(view.tfBuscarEmpresa.getText().length() > 0){
                    try {
                        listarEmpresa(model.buscarEmpresa(view.tfBuscarEmpresa.getText()));
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }else {
                    try {
                        listarEmpresa();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        view.tfBuscarTrabajador.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(view.tfBuscarTrabajador.getText().length() > 0){
                    try {
                        listarTrabajadores(model.buscarTrabajador(view.tfBuscarTrabajador.getText()));
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    public void limpiarCajas(){
        view.tfProductoNombre.setText("");
        view.tfProductoPrecio.setText("0");
        view.tfBuscarProducto.setText("");

        view.tfEmpresaNombre.setText("");
        view.tfBuscarEmpresa.setText("");
        view.tfEmpresaTelefono.setText("");
        view.tfEmpresaDireccion.setText("");
        view.dcEmpresaFundacion.setDate(null);
        view.cbEmpresaProducto.setSelectedIndex(0);
        view.cbEmpresaTipo.setSelectedIndex(0);

        view.tfTrabajadorNombre.setText("");
        view.tfTrabajadorApellidos.setText("");
        view.tfTrabajadorTelefono.setText("");
        view.tfBuscarTrabajador.setText("");
        view.dtTrabajadorNacimiento.setDate(null);
    }

    public void iniciarBotonProducto(boolean estado){
        view.btProductoEliminar.setEnabled(!estado);
        view.btProductoNuevo.setEnabled(estado);
        view.btProductoGuardar.setEnabled(!estado);
        view. btProductoModificar.setEnabled(!estado);
        view. btProductoCancelar.setEnabled(!estado);
        view.tfProductoNombre.setEnabled(!estado);
        view.tfProductoPrecio.setEnabled(!estado);
    }

    public void iniciarBotonEmpresa(boolean estado){
        view. btEmpresaEliminar.setEnabled(!estado);
        view. btEmpresaNuevo.setEnabled(estado);
        view.btEmpresaGuardar.setEnabled(!estado);
        view.btEmpresaModificar.setEnabled(!estado);
        view. btEmpresaCancelar.setEnabled(!estado);
        view.tfEmpresaNombre.setEnabled(!estado);
        view.tfEmpresaTelefono.setEnabled(!estado);
        view.tfEmpresaDireccion.setEnabled(!estado);
        view.cbEmpresaTipo.setEnabled(!estado);
        view.dcEmpresaFundacion.setEnabled(!estado);
        view.cbEmpresaProducto.setEnabled(!estado);
    }

    public void inicarBotonTrabajador(boolean estado){
        view.btTrabajadorEliminar.setEnabled(!estado);
        view.btTrabajadorNuevo.setEnabled(estado);
        view.btTrabajadorGuardar.setEnabled(!estado);
        view.btTrabajadorModificar.setEnabled(!estado);
        view. btTrabajadorCancelar.setEnabled(!estado);
        view.tfTrabajadorNombre.setEnabled(!estado);
        view.tfTrabajadorApellidos.setEnabled(!estado);
        view.tfTrabajadorTelefono.setEnabled(!estado);
        view.cbTrabajadorEmpresa.setEnabled(!estado);
        view.dtTrabajadorNacimiento.setEnabled(!estado);
    }

    public void apagarBotones(boolean estado){
        view.btProductoEliminar.setEnabled(!estado);
        view.btProductoNuevo.setEnabled(estado);
        view.btProductoGuardar.setEnabled(!estado);
        view. btProductoModificar.setEnabled(!estado);
        view. btProductoCancelar.setEnabled(!estado);
        view.tfProductoNombre.setEnabled(!estado);
        view.tfProductoPrecio.setEnabled(!estado);

        view. btEmpresaEliminar.setEnabled(!estado);
        view. btEmpresaNuevo.setEnabled(estado);
        view.btEmpresaGuardar.setEnabled(!estado);
        view.btEmpresaModificar.setEnabled(!estado);
        view. btEmpresaCancelar.setEnabled(!estado);
        view.tfEmpresaNombre.setEnabled(!estado);
        view.tfEmpresaTelefono.setEnabled(!estado);
        view.tfEmpresaDireccion.setEnabled(!estado);
        view.cbEmpresaTipo.setEnabled(!estado);
        view.dcEmpresaFundacion.setEnabled(!estado);
        view.cbEmpresaProducto.setEnabled(!estado);

        view.btTrabajadorEliminar.setEnabled(!estado);
        view.btTrabajadorNuevo.setEnabled(estado);
        view.btTrabajadorGuardar.setEnabled(!estado);
        view.btTrabajadorModificar.setEnabled(!estado);
        view.btTrabajadorCancelar.setEnabled(!estado);
        view.dtTrabajadorNacimiento.setEnabled(!estado);
        view.tfTrabajadorNombre.setEnabled(!estado);
        view.tfTrabajadorApellidos.setEnabled(!estado);
        view.tfTrabajadorTelefono.setEnabled(!estado);
        view.cbTrabajadorEmpresa.setEnabled(!estado);

    }

    public void listarProducto() throws ParseException {
        List<Producto> filaProducto = model.getProductos();
        view.modeloTablaProducto.setNumRows(0);
        view.cbEmpresaProducto.removeAllItems();
        if(filaProducto != null){
            for(Producto producto : filaProducto){

                Object[] fila = new Object[]{producto.get_id(),producto.getNombre(),util.formatMoneda(producto.getPrecioBase())};

                view.modeloTablaProducto.addRow(fila);
                view.cbEmpresaProducto.addItem(producto);
            }
        }
        if(filaProducto.size()>0){
            view.btProductoModificar.setEnabled(true);
        }

    }

    public void listarProducto(List<Producto> filaProducto) throws ParseException {
        view.modeloTablaProducto.setNumRows(0);;
        if(filaProducto != null){
            for(Producto producto : filaProducto){

                Object[] fila = new Object[]{producto.get_id(),producto.getNombre(),util.formatMoneda(producto.getPrecioBase())};

                view.modeloTablaProducto.addRow(fila);
            }
        }
    }

    public void listarTrabajadores(){
        try {
            List<Trabajador> filaTrabajador = model.getTrabajadores();
            for (Trabajador trabajador : filaTrabajador) {
                System.out.println(trabajador.getNombre()+" CONTROLLER "+trabajador.getEmpresaAsignada());
            }
            view.modeloTablaTrabajador.setNumRows(0);
            if (filaTrabajador != null) {
                for (Trabajador trabajador : filaTrabajador) {
                    if(trabajador.getEmpresaAsignada() == null){
                        trabajador.setEmpresaAsignada(null);
                    }
                    Object[] fila = new Object[]{trabajador.get_id(),trabajador.getNombre(), trabajador.getApellidos()
                            , trabajador.getTelefono(), trabajador.getEmpresaAsignada(), Util.formatFecha(trabajador.getNacimiento())};
                    view.modeloTablaTrabajador.addRow(fila);
                }
            }else {
                view.lbEstado.setText("No hay trabajadores registrados");
            }

        }catch (Exception e){
            view.lbEstado.setText("Algo ha pasado :(");
        }

    }

    public void listarTrabajadores(List<Trabajador> filaTrabajador){
            view.modeloTablaTrabajador.setNumRows(0);
            if (filaTrabajador != null) {
                for (Trabajador trabajador : filaTrabajador) {
                    if (trabajador.getEmpresaAsignada() == null) {
                        trabajador.setEmpresaAsignada(null);
                    }
                    Object[] fila = new Object[]{trabajador.get_id(), trabajador.getNombre(), trabajador.getApellidos()
                            , trabajador.getTelefono(), trabajador.getEmpresaAsignada(), Util.formatFecha(trabajador.getNacimiento())};
                    view.modeloTablaTrabajador.addRow(fila);
                }
            }

    }

    public void listarEmpresa() throws ParseException {
        List<Empresa> filaEmpresa = model.getEmpresas();
        System.out.println(filaEmpresa);
        view.modeloTablaEmpresa.setNumRows(0);
        view.cbTrabajadorEmpresa.removeAllItems();
        if(filaEmpresa != null){
            for(Empresa empresa : filaEmpresa){
                if(empresa.getProducto() == null)
                    empresa.setProducto(null);
                Object[] fila = new Object[]{empresa.get_id(),empresa.getNombre(),empresa.getTipo(),
                        empresa.getDireccion(),empresa.getTelefono(),
                        Util.formatFecha(empresa.getFundacion()),empresa.getProducto()};

                view.modeloTablaEmpresa.addRow(fila);
                view.cbTrabajadorEmpresa.addItem(empresa);
            }
        }
    }

    public void listarEmpresa(List<Empresa> filaEmpresa) throws ParseException {
        view.modeloTablaEmpresa.setNumRows(0);
        if(filaEmpresa != null){
            for(Empresa empresa : filaEmpresa){
                if(empresa.getProducto() == null)
                    empresa.setProducto(null);
                Object[] fila = new Object[]{empresa.get_id(),empresa.getNombre(),empresa.getTipo(),
                        empresa.getDireccion(),empresa.getTelefono(),
                        Util.formatFecha(empresa.getFundacion()),empresa.getProducto()};

                view.modeloTablaEmpresa.addRow(fila);
            }
        }
    }

    private void registroUsuario(){
        Registro registro = new Registro();
        registro.setVisible(true);
        usuario = registro.getUser();
        model.addUsuario(usuario);
    }

    private void login(){
        JLogin login = new JLogin();
        login.setVisible(true);

        String user = login.getUsuario();
        String contrasena = login.getContrasena();

        usuario = model.getUsuario(user);
        if(!usuario.getPassword().equals(contrasena)){
            JOptionPane.showMessageDialog(null, "Datos incorrectos","Login", JOptionPane.ERROR_MESSAGE);
            login.setVisible(true);
            return;
        }else {
            try {
                inicializar();
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }

    }

}
