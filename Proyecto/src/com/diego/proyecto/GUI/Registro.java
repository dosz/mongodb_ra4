package com.diego.proyecto.GUI;

import com.diego.proyecto.Objetos.Usuario;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Registro extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfUsuario;
    private JTextField tfPassword;
    private VentanaModel model;
    private String usuario;
    private String password;
    private Usuario user;

    public Registro() {
        setContentPane(contentPane);
        setSize(new Dimension(400, 200));
        setTitle("¡Registrate Gratis!");
        setLocationRelativeTo(null);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        user = new Usuario();
        user.setNombre(tfUsuario.getText());
        user.setPassword(tfPassword.getText());
        setUser(user);
        setVisible(false);
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    private void onCancel() {
        setVisible(false);
    }

    public static void main(String[] args) {
        Registro dialog = new Registro();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
