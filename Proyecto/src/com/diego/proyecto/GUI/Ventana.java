package com.diego.proyecto.GUI;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * GUI Principal
 */
public class Ventana extends JFrame{
    public JPanel panel1;
    public JTabbedPane tabbedPane1;
    public JTextField tfEmpresaNombre;
    public JTextField tfEmpresaDireccion;
    public JTextField tfEmpresaTelefono;
    public JComboBox cbEmpresaTipo;
    public JButton btEmpresaNuevo;
    public JButton btEmpresaModificar;
    public JButton btEmpresaGuardar;
    public JDateChooser dcEmpresaFundacion;
    public JButton btEmpresaCancelar;
    public JButton btEmpresaEliminar;
    public JTextField tfProductoNombre;
    public JTextField tfProductoPrecio;
    public JButton btProductoNuevo;
    public JButton btProductoModificar;
    public JButton btProductoGuardar;
    public JButton btProductoCancelar;
    public JButton btProductoEliminar;
    public JTextField tfTrabajadorNombre;
    public JTextField tfTrabajadorApellidos;
    public JTextField tfTrabajadorTelefono;
    public JComboBox cbTrabajadorEmpresa;
    public JTable tablaTrabajador;
    public JButton btTrabajadorNuevo;
    public JButton btTrabajadorModificar;
    public JButton btTrabajadorGuardar;
    public JButton btTrabajadorCancelar;
    public JButton btTrabajadorEliminar;
    public JMenuBar menuBar;
    public JTable tablaProducto;
    public JTable tablaEmpresa;
    public JLabel lbEstado;
    public JDateChooser dtTrabajadorNacimiento;
    public JTextField tfBuscarProducto;
    public JTextField tfBuscarEmpresa;
    public JTextField tfBuscarTrabajador;
    public JComboBox cbEmpresaProducto;
    public JMenu menuArchivo;
    public JMenuItem botonImportarSQL;
    public JMenuItem botonExportarXML;
    public JMenuItem botonConectar;
    public JMenuItem botonDesconectar;
    public JMenuItem botonSalir;
    public JMenuItem botonRegistro;
    public DefaultTableModel modeloTablaProducto;
    public DefaultTableModel modeloTablaEmpresa;
    public DefaultTableModel modeloTablaTrabajador;
    public JMenuItem botonSQL;

    public Ventana() throws Exception{
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        setSize((int) width, (int) height);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        iniciarBotones(true);
        setJMenuBar(menuBar());
        iniciarTablas();
        cbEmpresaTipo.addItem("SL");
        cbEmpresaTipo.addItem("SA");
        Font font = new Font("Ubuntu", Font.BOLD, 18);
        lbEstado.setFont(font);
        setVisible(true);
    }

    public JMenuBar menuBar(){
        menuBar = new JMenuBar();

        menuArchivo = new JMenu("Archivo");

        menuBar.add(menuArchivo);

        botonExportarXML = new JMenuItem("Exportar XML");
        botonSQL = new JMenuItem("Exportar a SQL");
        botonImportarSQL = new JMenuItem("Importar a SQL");
        botonConectar = new JMenuItem("Conectar");
        botonDesconectar = new JMenuItem("Desconectar");
        botonRegistro = new JMenuItem("Registrarse");
        botonSalir = new JMenuItem("Salir");

        menuArchivo.add(botonConectar);
        menuArchivo.add(botonRegistro);
        menuArchivo.add(botonDesconectar);
        menuArchivo.add(botonSQL);
        menuArchivo.add(botonExportarXML);
        menuArchivo.add(botonImportarSQL);
        menuArchivo.add(botonSalir);

        return menuBar;
    }

    public void iniciarBotones(boolean estado){
        btProductoEliminar.setEnabled(!estado);
        btProductoNuevo.setEnabled(!estado);
        btProductoGuardar.setEnabled(!estado);
        btProductoModificar.setEnabled(!estado);
        btProductoCancelar.setEnabled(!estado);
        tfProductoNombre.setEnabled(!estado);
        tfProductoPrecio.setEnabled(!estado);

        btEmpresaEliminar.setEnabled(!estado);
        btEmpresaNuevo.setEnabled(!estado);
        btEmpresaGuardar.setEnabled(!estado);
        btEmpresaModificar.setEnabled(!estado);
        btEmpresaCancelar.setEnabled(!estado);
        tfEmpresaNombre.setEnabled(!estado);
        tfEmpresaTelefono.setEnabled(!estado);
        tfEmpresaDireccion.setEnabled(!estado);
        cbEmpresaTipo.setEnabled(!estado);
        dcEmpresaFundacion.setEnabled(!estado);
        cbEmpresaProducto.setEnabled(!estado);

        btTrabajadorEliminar.setEnabled(!estado);
        btTrabajadorNuevo.setEnabled(!estado);
        btTrabajadorGuardar.setEnabled(!estado);
        btTrabajadorModificar.setEnabled(!estado);
        btTrabajadorCancelar.setEnabled(!estado);
        dtTrabajadorNacimiento.setEnabled(!estado);
        tfTrabajadorNombre.setEnabled(!estado);
        tfTrabajadorApellidos.setEnabled(!estado);
        tfTrabajadorTelefono.setEnabled(!estado);
        cbTrabajadorEmpresa.setEnabled(!estado);


    }

    public void iniciarTablas(){
        String[] productos = new String[]{"id","nombre","precio"};
        String[] empresas = new String[]{"id","nombre","tipo","direccion","telefono","fundacion","producto"};
        String[] trabajadores = new String[]{"id","nombre","apellidos","telefono","empresa","nacimiento"};

        modeloTablaProducto = new DefaultTableModel(productos,0);
        tablaProducto.setModel(modeloTablaProducto);

        modeloTablaEmpresa = new DefaultTableModel(empresas,0);
        tablaEmpresa.setModel(modeloTablaEmpresa);

        modeloTablaTrabajador = new DefaultTableModel(trabajadores,0);
        tablaTrabajador.setModel(modeloTablaTrabajador);

    }

}
