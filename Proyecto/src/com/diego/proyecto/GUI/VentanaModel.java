package com.diego.proyecto.GUI;

import com.diego.proyecto.Constantes;
import com.diego.proyecto.HibernateUtil;
import com.diego.proyecto.Objetos.Empresa;
import com.diego.proyecto.Objetos.Producto;
import com.diego.proyecto.Objetos.Trabajador;
import com.diego.proyecto.Objetos.Usuario;
import com.diego.proyecto.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by dos_6 on 03/02/2016.
 */
public class VentanaModel {

    List<Producto> listaProductos;
    List<Empresa> listaEmpresas;
    List<Trabajador> listaTrabajadores;
    MongoClient mongoClient;
    MongoDatabase db;

    public VentanaModel(){
        listaProductos = new ArrayList<>();
        listaEmpresas = new ArrayList<>();
        listaTrabajadores = new ArrayList<>();
    }

    public void conectar() {
        try {
            HibernateUtil.buildSessionFactory();
            HibernateUtil.openSession();

        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void desconectar() {
        try {
            HibernateUtil.closeSessionFactory();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void conectarMongo(){
        mongoClient = new MongoClient();
        db = mongoClient.getDatabase(Constantes.NOMBRE);
    }

    public void desconectarMongo(){
        mongoClient.close();
    }

    public void addTrabajador(Trabajador trabajador) throws ParseException {
        List<Trabajador> _id = getTrabajadores();
        int id;
        if(_id.size() == 0){
            id = 0;
        }else{
            id = _id.get(_id.size()-1).get_id()+1;
        }
        org.bson.Document document = new org.bson.Document()
                .append("_id", String.valueOf(id))
                .append("nombre", trabajador.getNombre())
                .append("apellidos", trabajador.getApellidos())
                .append("nacimiento", Util.formatFecha(trabajador.getNacimiento()))
                .append("telefono", trabajador.getTelefono())
                .append("id_empresa", String.valueOf(trabajador.getEmpresaAsignada().get_id()));
        db.getCollection(Trabajador.COLECCION).insertOne(document);
    }

    public void addProducto(Producto producto) throws ParseException {
        List<Producto> _id = getProductos();
        int id;
        if(_id.size() == 0){
            id = 0;
        }else{
            id = _id.get(_id.size()-1).get_id()+1;
        }
        org.bson.Document document = new org.bson.Document()
                .append("_id", String.valueOf(id))
                .append("nombre", producto.getNombre())
                .append("precio_base", String.valueOf(producto.getPrecioBase()));
        db.getCollection(Producto.COLECCION).insertOne(document);
    }

    public void addEmpresa(Empresa empresa) throws ParseException {
        List<Empresa> _id = getEmpresas();
        int id;
        if(_id.size() == 0){
            id = 0;
        }else {
            id = _id.get(_id.size() - 1).get_id() + 1;
        }
        org.bson.Document document = new org.bson.Document()
                .append("_id", String.valueOf(id))
                .append("nombre", empresa.getNombre())
                .append("direccion", empresa.getDireccion())
                .append("telefono", empresa.getTelefono())
                .append("fundacion", Util.formatFecha(empresa.getFundacion()))
                .append("id_producto", String.valueOf(empresa.getProducto().get_id()))
                .append("tipo", empresa.getTipo());
        db.getCollection(Empresa.COLECCION).insertOne(document);
    }

    public void addUsuario(Usuario usuario){
        org.bson.Document document = new org.bson.Document()
                .append("nombre", usuario.getNombre())
                .append("password", usuario.getPassword());
        db.getCollection(Usuario.COLECCION).insertOne(document);
    }

    public void eliminarTrabajador(String nombre) {
        db.getCollection(Trabajador.COLECCION).deleteOne(new org.bson.Document("nombre", nombre));
    }

    public void eliminarEmpresa(String nombre) {
        db.getCollection(Empresa.COLECCION).deleteOne(new org.bson.Document("nombre", nombre));
    }

    public void eliminarProducto(String nombre) {
        db.getCollection(Producto.COLECCION).deleteOne(new org.bson.Document("nombre", nombre));
    }

    public List<Producto> getProductos() throws ParseException {
        FindIterable<org.bson.Document> findIterable = db.getCollection(Producto.COLECCION).find();
        return getListaProductos(findIterable);
    }

    public List<Empresa> getEmpresas() throws ParseException {
        FindIterable<org.bson.Document> findIterable = db.getCollection(Empresa.COLECCION).find();
        return getListaEmpresa(findIterable);
    }

    public List<Trabajador> getTrabajadores() throws ParseException {
        FindIterable<org.bson.Document> findIterable = db.getCollection(Trabajador.COLECCION).find();
        return getListaTrabajadores(findIterable);
    }

    public List<Producto> getListaProductos(FindIterable<org.bson.Document> findIterable) throws ParseException {

        List<Producto> productosList = new ArrayList<>();
        Producto producto = null;
        Iterator<org.bson.Document> iter = findIterable.iterator();

        while(iter.hasNext()){
            org.bson.Document documento = iter.next();
            producto = new Producto();
            producto.set_id(Integer.parseInt(documento.getString("_id")));
            producto.setNombre(documento.getString("nombre"));
            producto.setPrecioBase(Float.parseFloat(documento.getString("precio_base")));
            productosList.add(producto);
        }
        return productosList;
    }

    public List<Trabajador> getListaTrabajadores(FindIterable<org.bson.Document> findIterable) throws ParseException {

        List<Trabajador> trabajadorList = new ArrayList<>();
        Trabajador trabajador = null;
        Iterator<org.bson.Document> iter = findIterable.iterator();

        while(iter.hasNext()){
            org.bson.Document documento = iter.next();
            trabajador = new Trabajador();
            trabajador.set_id(Integer.parseInt(documento.getString("_id")));
            trabajador.setNombre(documento.getString("nombre"));
            trabajador.setApellidos(documento.getString("apellidos"));
            trabajador.setNacimiento(Util.parseFecha(documento.getString("nacimiento")));
            trabajador.setTelefono(documento.getString("telefono"));
            trabajador.setEmpresaAsignada(getEmpresa(Integer.parseInt(documento.getString("id_empresa"))));
            System.out.println("EMPRESA DE TOÑO " + trabajador.getEmpresaAsignada());
            trabajadorList.add(trabajador);
        }
        return trabajadorList;
    }

    public List<Empresa> getListaEmpresa(FindIterable<org.bson.Document> findIterable) throws ParseException {

        List<Empresa> empresaList = new ArrayList<>();
        Empresa empresa = null;
        Iterator<org.bson.Document> iter = findIterable.iterator();
        while (iter.hasNext()){
            org.bson.Document documento = iter.next();
            empresa = new Empresa();
            empresa.set_id(Integer.parseInt(documento.getString("_id")));
            empresa.setNombre(documento.getString("nombre"));
            empresa.setDireccion(documento.getString("direccion"));
            empresa.setTelefono(documento.getString("telefono"));
            empresa.setProducto(getProducto(Integer.parseInt(documento.getString("id_producto"))));
            try {
                empresa.setFundacion(Util.parseFecha(documento.getString("fundacion")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            empresa.setTipo(documento.getString("tipo"));
            empresaList.add(empresa);
        }

        return empresaList;

    }

    public Producto getProducto(int _id) throws ParseException {
        FindIterable<org.bson.Document> findIterable = db.getCollection(Producto.COLECCION)
                .find(new org.bson.Document("_id", String.valueOf(_id)));
        org.bson.Document documento = findIterable.first();
        return getProducto(documento);
    }

    public Empresa getEmpresa(int _id) throws ParseException {

        FindIterable<org.bson.Document> findIterable = db.getCollection(Empresa.COLECCION)
                .find(new org.bson.Document("_id", String.valueOf(_id)));
        org.bson.Document documento = findIterable.first();
        return getEmpresa(documento);
    }

    public Trabajador getTrabajador(int _id) throws ParseException {
        FindIterable<org.bson.Document> findIterable = db.getCollection(Trabajador.COLECCION)
                .find(new org.bson.Document("_id", String.valueOf(_id)));
        org.bson.Document documento = findIterable.first();
        return getTrabajador(documento);
    }

    public Usuario getUsuario(String usuario){
        FindIterable<org.bson.Document> findIterable = db.getCollection(Usuario.COLECCION)
                .find(new org.bson.Document("nombre", usuario));
        org.bson.Document documento = findIterable.first();
        return getUsuario(documento);
    }

    public Usuario getUsuario(org.bson.Document documento){
        Usuario usuario = new Usuario();
        usuario.setNombre(documento.getString("nombre"));
        usuario.setPassword(documento.getString("password"));
        return usuario;
    }

    public Producto getProducto(org.bson.Document documento){
        Producto producto = new Producto();
        producto.set_id(Integer.parseInt(documento.getString("_id")));
        producto.setNombre(documento.getString("nombre"));
        producto.setPrecioBase(Float.parseFloat(documento.getString("precio_base")));
        return producto;
    }

    public Trabajador getTrabajador(org.bson.Document documento) throws ParseException {
        Trabajador trabajador = new Trabajador();
        trabajador.set_id(Integer.parseInt(documento.getString("_id")));
        trabajador.setNombre(documento.getString("nombre"));
        trabajador.setNacimiento(Util.parseFecha(documento.getString("nacimiento")));
        trabajador.setTelefono(documento.getString("telefono"));
        trabajador.setApellidos(documento.getString("apellidos"));
        trabajador.setEmpresaAsignada(getEmpresa(Integer.parseInt(documento.getString("id_empresa"))));
        return trabajador;
    }

    public Empresa getEmpresa(org.bson.Document document) throws ParseException {
        Empresa empresa = new Empresa();
        empresa.set_id(Integer.parseInt(document.getString("_id")));
        empresa.setNombre(document.getString("nombre"));
        empresa.setTelefono(document.getString("telefono"));
        empresa.setDireccion(document.getString("direccion"));
        empresa.setProducto(getProducto(Integer.parseInt(document.getString("id_producto"))));
        try {
            empresa.setFundacion(Util.parseFecha(document.getString("fundacion")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        empresa.setTipo(document.getString("tipo"));
        return empresa;
    }

    public void updateProducto(Producto producto){
        db.getCollection(Producto.COLECCION).replaceOne(new org.bson.Document("nombre", producto.getNombre()),
                new org.bson.Document()
                        .append("nombre", producto.getNombre())
                        .append("precio_base", String.valueOf(producto.getPrecioBase()))
        );
    }

    public void updateEmpresa(Empresa empresa){
        db.getCollection(Empresa.COLECCION).replaceOne(new org.bson.Document("nombre", empresa.getNombre())
                ,new org.bson.Document()
                .append("nombre", empresa.getNombre())
                .append("direccion", empresa.getDireccion())
                .append("telefono", empresa.getTelefono())
                .append("fundacion", Util.formatFecha(empresa.getFundacion()))
                .append("id_producto", String.valueOf(empresa.getProducto().get_id()))
                        .append("tipo", empresa.getTipo())

        );
    }

    public void updateTrabajador(Trabajador trabajador){
        db.getCollection(Trabajador.COLECCION).replaceOne(new org.bson.Document("nombre", trabajador.getNombre()),
                new org.bson.Document()
                        .append("nombre", trabajador.getNombre())
                        .append("apellidos", trabajador.getApellidos())
                        .append("nacimiento", Util.formatFecha(trabajador.getNacimiento()))
                        .append("telefono", trabajador.getTelefono())
                        .append("id_empresa", String.valueOf(trabajador.getEmpresaAsignada().get_id()))
        );
    }

    public List<Producto> buscarProducto(String parametro) throws ParseException {
        System.out.println(parametro);
            org.bson.Document documento = new org.bson.Document("$or", Arrays.asList(
                    new org.bson.Document("nombre", parametro),
                    new org.bson.Document("precio_base", parametro)));

            FindIterable findIterable = db.getCollection(Producto.COLECCION)
                    .find(documento);

        return getListaProductos(findIterable);
    }

    public List<Empresa> buscarEmpresa(String parametro) throws ParseException {

        org.bson.Document documento = new org.bson.Document("$or", Arrays.asList(
                new org.bson.Document("nombre", parametro),
                new org.bson.Document("direccion", parametro),
                new org.bson.Document("telefono",parametro),
                new org.bson.Document("fundacion",parametro),
                new org.bson.Document("tipo", parametro)));

        FindIterable findIterable = db.getCollection(Empresa.COLECCION)
                .find(documento);

        return getListaEmpresa(findIterable);
    }

    public List<Trabajador> buscarTrabajador(String parametro) throws ParseException {

        org.bson.Document documento = new org.bson.Document("$or", Arrays.asList(
                new org.bson.Document("nombre", parametro),
                new org.bson.Document("apellidos", parametro),
                new org.bson.Document("telefono",parametro),
                new org.bson.Document("nacimiento",parametro)));

        FindIterable findIterable = db.getCollection(Trabajador.COLECCION)
                .find(documento);

        return getListaTrabajadores(findIterable);
    }

    public void guardarHIbernate(Object objeto) throws HibernateException{
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(objeto);
        session.getTransaction().commit();
        session.close();
    }

    public List<Producto> getProductoHibernate(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Producto");
        listaProductos = query.list();

        return listaProductos;
    }

    public void XMLEmpresa(List<Empresa> vectorEmpresa){

        //Se crea la variable para el documento
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            //Se crea el documento XML
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            //Se crea el elemento raiz
            org.w3c.dom.Element raiz = documento.createElement("Empresas");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoEmpresa = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            //Se crea el nodo padre
            for (Empresa empresa : vectorEmpresa) { // Se recorren los vectores con un bucle.
                nodoEmpresa = documento.createElement("Empresa");
                raiz.appendChild(nodoEmpresa);
                //Se crean los nodos hijos

                nodoDatos = documento.createElement("id");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(empresa.getId()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nombre");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("tipo");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getTipo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("direccion");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getDireccion());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("telefono");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(empresa.getTelefono());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("fundacion");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(Util.formatFecha(empresa.getFundacion()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("id_producto");
                nodoEmpresa.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(empresa.getProducto().get_id()));
                nodoDatos.appendChild(texto);


            }
            //Se guardan los datos en un documento XML
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File(System.getProperty("user.home")+File.separator+"empresa.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    public void XMLTrabajador(List<Trabajador> vectorTrabajadores){

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            org.w3c.dom.Element raiz = documento.createElement("Trabajadores");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoTrabajador = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            for (Trabajador trabajador : vectorTrabajadores) {
                nodoTrabajador = documento.createElement("Trabajador");
                raiz.appendChild(nodoTrabajador);

                nodoDatos = documento.createElement("id");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(trabajador.getId()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nombre");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("apellidos");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(trabajador.getApellidos());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nacimiento");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(Util.formatFecha(trabajador.getNacimiento()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("id_empresa");
                nodoTrabajador.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(trabajador.getEmpresaAsignada().get_id()));
                nodoDatos.appendChild(texto);



            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File(System.getProperty("user.home")+File.separator+"trabajadores.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    public void XMLProducto(List<Producto> vectorProductos){

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            org.w3c.dom.Element raiz = documento.createElement("Productos");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoProducto = null, nodoDatos = null;
            org.w3c.dom.Text texto = null;
            for (Producto producto : vectorProductos) {
                nodoProducto = documento.createElement("Producto");
                raiz.appendChild(nodoProducto);

                nodoDatos = documento.createElement("id");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(producto.getId()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("nombre");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(producto.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("precio_base");
                nodoProducto.appendChild(nodoDatos);

                texto = documento.createTextNode(String.valueOf(producto.getPrecioBase()));
                nodoDatos.appendChild(texto);

            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File(System.getProperty("user.home")+File.separator+"productos.xml"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }
}
