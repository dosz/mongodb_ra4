import com.diego.proyecto.GUI.Ventana;
import com.diego.proyecto.GUI.VentanaController;
import com.diego.proyecto.GUI.VentanaModel;

import javax.swing.*;

/**
 * Created by dos_6 on 05/02/2016.
 */
public class Main {

    public static void main (String args[]) throws Exception {
        UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
        Ventana ventana = new Ventana();
        VentanaModel model = new VentanaModel();
        VentanaController ventanaController = new VentanaController(ventana, model);
    }

}
