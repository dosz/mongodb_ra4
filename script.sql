create database if not exists mongoDB;
use mongoDB;

create table if not exists productos(
    id int unsigned auto_increment primary key, 
    _id int,
    nombre varchar(50) not null,
    precio_base float not null default 0  
    
);

create table if not exists empresas (
    id int unsigned auto_increment primary key, 
    _id int,
    nombre varchar(50) not null,
    direccion varchar(50) not null,
    telefono varchar(50) not null,
    fundacion Date not null,
	id_producto int,
    tipo varchar(50) not null
       
);

create table if not exists trabajadores(
  
	id int unsigned auto_increment primary key, 
   _id int,
  nombre varchar(50) not null,
  apellidos varchar(50) not null,
  nacimiento date not null,
  empresa varchar(50) not null,
  telefono varchar(50) not null,
  id_empresa int unsigned
);

create table if not exists usuarios(
  	id int unsigned auto_increment primary key, 
    _id int,
    nombre varchar(50) not null,
    pass varchar(50) not null
    
);
